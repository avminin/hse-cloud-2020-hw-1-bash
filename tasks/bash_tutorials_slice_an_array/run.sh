#!/bin/bash
array=($(cat))
for (( i = 3; i < 8; i++ )); do
    if [ $i -lt 7 ]; then
      echo -ne "${array[${i}]} "
    else
      printf "${array[${i}]}\n"
    fi
done