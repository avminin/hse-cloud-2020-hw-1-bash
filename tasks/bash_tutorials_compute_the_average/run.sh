#!/bin/bash
awk 'NR==1{lines=$1;next}
{total+=$1}
END {printf("%.3f\n",total/lines)}
'
