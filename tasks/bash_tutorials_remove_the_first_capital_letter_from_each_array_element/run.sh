#!/bin/bash
array=( $(cat) )
counter=${#array[@]}
for country in ${array[@]};
do
    counter=$((counter - 1))
    if [ $counter -gt 0 ]; then
      echo -ne ".${country:1} "
    else
      printf ".${country:1}\n"
    fi
done